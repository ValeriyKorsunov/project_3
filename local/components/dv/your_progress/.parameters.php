<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$ext = 'wmv,wma,flv,vp6,mp3,mp4,aac,jpg,jpeg,gif,png';
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "TEMPLATE_FOR_TITLE" => array(
            "PARENT" => "BASE",
            "NAME" => "Заголовок",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Заголовок",
            "REFRESH" => "Y",
        ),
        "TEMPLATE_FOR_TEXT" => array(
            "PARENT" => "BASE",
            "NAME" => "Текст ниже",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Текст ниже",
            "REFRESH" => "Y",
        ),
    ),
);