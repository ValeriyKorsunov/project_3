<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*
$arResult['title'] = date($arParams["TEMPLATE_FOR_TITLE"]);
$arResult['TEXT'] = date($arParams["TEMPLATE_FOR_TEXT"]);

$this->IncludeComponentTemplate();
*/
?>

<h2 class="achievements__item-title"><?=$arParams["TEMPLATE_FOR_TITLE"]?></h2>
<div class="achievements__item-text"><?=$arParams["TEMPLATE_FOR_TEXT"]?></div>