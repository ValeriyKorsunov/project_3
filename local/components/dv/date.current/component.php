<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['title'] = date($arParams["TEMPLATE_FOR_TITLE"]);
$arResult['button'] = date($arParams["TEMPLATE_FOR_BUTTON"]);
$arResult['link'] = date($arParams["TEMPLATE_FOR_link"]);
$arResult['img'] = date($arParams["TEMPLATE_FOR_IMG"]);

$this->IncludeComponentTemplate();
