<?
      if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

      $arComponentDescription = array(
          "NAME" => GetMessage("Узнать больше"),
          "DESCRIPTION" => GetMessage("Вывод кнопки узнать больше"),
          "ICON" => "/images/cat_all.gif",
          "PATH" => array(
              "ID" => "ПроКлуб",
          ),
      );
