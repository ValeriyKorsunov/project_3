<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$ext = 'wmv,wma,flv,vp6,mp3,mp4,aac,jpg,jpeg,gif,png';
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "TEMPLATE_FOR_TITLE" => array(
            "PARENT" => "BASE",
            "NAME" => "Заголовок",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Заголовок",
            "REFRESH" => "Y",
        ),
        "TEMPLATE_FOR_BUTTON" => array(
            "PARENT" => "BASE",
            "NAME" => "Название кнопки",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Узнать больше",
            "REFRESH" => "Y",
        ),
        "TEMPLATE_FOR_link" => array(
            "PARENT" => "BASE",
            "NAME" => "Ссылка",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "/",
            "REFRESH" => "Y",
        ),
        "TEMPLATE_FOR_IMG" =>Array(
            "PARENT" => "BASE_SETTINGS",
            "NAME" => 'Выберите файл:',
            "TYPE" => "FILE",
            "FD_TARGET" => "F",
            "FD_EXT" => $ext,
            "FD_UPLOAD" => true,
            "FD_USE_MEDIALIB" => true,
            "FD_MEDIALIB_TYPES" => Array('video', 'sound')
        ),
    ),
);