jQuery(document).ready(function($){
    //Dots
    $(".articles__text").dotdotdot();

    // Tabs
    $('.js-tabs-link').click(function(e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $(this).addClass('active');
        $(this).parent().siblings().find('.js-tabs-link').removeClass('active');
        $(this).parents('.js-tabs').find('.js-tab').removeClass('active');
        $(target).addClass('active');
    });
    
    // Dates
    $('.date, .time, .reservation__slider-item').not('.disabled').click(function() {
        $(this).parent().find('.active').removeClass('active');
        $(this).toggleClass('active');
    });
    
    // Hero slider
    $('.js-hero-slider').slick({
      dots: true,
      infinite: true,
      autoplay: true,
      fade:true,
      autoplaySpeed: 5000,
    });  
  
    // Courts slider
    $('.js-courts-slider').slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 3,
    });
    
    $(window).resize(function() {
        $('.js-hero-slider').slick('resize');
        $('.js-courts-slider').slick('resize');
    });

    $(window).on('orientationchange', function() {
        $('.js-hero-slider').slick('resize');
        $('.js-courts-slider').slick('resize');
    });
    
    // Gallery
	$('.gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-no-margins mfp-with-zoom',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
    
    $('.js-modal-toggle').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
        
	    fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
    });
    
});
    