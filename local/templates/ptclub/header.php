<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$isIndexPage = $APPLICATION->GetCurPage(true) == SITE_DIR."index.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?$APPLICATION->ShowHead();?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?$APPLICATION->ShowTitle();?></title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?
        use Bitrix\Main\Page\Asset;
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/magnific-popup.css');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH.'/css/main.css');
        //<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        Asset::getInstance()->addJs("https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/jquery.magnific-popup.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/jquery.dotdotdot.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/vendor/slick.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");
    ?>    
  </head>
<body>
    <?$APPLICATION->ShowPanel()?>
    <div class="page">
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">

                            <a href="/" class="header__logo-link">
                                <img src="<?=SITE_TEMPLATE_PATH?>/i/logo.png" alt="" class="header__logo-img">
                            </a>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-lg-9">
                        <div class="header__contacts">
                            <a href="mailto:<?=file_get_contents('include/mail_header.php');?>"
                               class="header__contacts-item">
                                <? $APPLICATION->IncludeFile(SITE_DIR."/include/mail_header.php",
                                                            array(),
                                                            array(MODE => "text")); ?>
                            </a>
                            <a href="tel:<?=file_get_contents('include/telephone_header.php');?>"
                               class="header__contacts-item header__tel tel">
                                <? $APPLICATION->IncludeFile(SITE_DIR."/include/telephone_header.php",
                                                            array(),
                                                            array(MODE => "text")); ?>
                            </a>
                            <a href="reservation.html" class="header__contacts-item header__button button button_primary">
                                Бронирование
                            </a>
                        </div>
                        <!-- МЕНЮ -->
                        <?$APPLICATION->IncludeComponent(
                                "bitrix:menu",
                            "menu",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(0 => ""),
                                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                                "USE_EXT" => "N"),
                            false );    ?>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </header><!-- /.header -->

        <main class="content">

        <?if($isIndexPage):?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news.list",
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(0=>"",1=>"",),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "1",
                    "IBLOCK_TYPE" => "slider",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "N",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(0=>"url",1=>"",),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC"
                )
            );?>
            <div class="promo__container container">
        <?else:?>
            <div class="container">
                <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
                    "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                    "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                    "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                ),
                    false
                );?>
            </div>

            <div class="container">

        <?endif;?>
