<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="courts">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="courts__item court box" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                <div class="court__img-block">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" class="court__img">
                </div>
            <?endif;?>
            <div class="court__content">
                <h2 class="court__title"><?echo $arItem["NAME"]?></h2>
                <?if($arItem["PREVIEW_TEXT"]):?>
                    <p class="court__text"><?echo $arItem["PREVIEW_TEXT"];?></p>
                <?endif;?>
                <div class="court__action">
                    <a href="" class="button button_primary button_width_300">Записаться</a>
                </div>
            </div>
        </div>
    <?endforeach;?>
</div>
