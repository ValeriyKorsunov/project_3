<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="hero js-hero-slider">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="hero__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="hero__img-block" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
            <div class="hero__container container">
                <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                    <h2 class="hero__title"><?echo $arItem["NAME"]?></h2>
                <?endif;?>
                <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                    <div class="hero__subtitle h2"><?echo $arItem["PREVIEW_TEXT"];?></div>
                <?endif;?>
                <?if($arItem["PROPERTIES"]["url"]["VALUE"]):?>
                    <div class="hero__action">
                        <a href="<?=$arItem["PROPERTIES"]["url"]["VALUE"];?>"
                           class="button button_primary button_width_300">Узнать больше</a>
                    </div>
                <?endif;?>
            </div><!-- /.container -->
        </div>
    <?endforeach;?>
</div>
