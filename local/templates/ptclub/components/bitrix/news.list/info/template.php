<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="articles row">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="articles__col col-lg-4">
            <a href="text.html" class="articles__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="articles__img-block" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
                <div class="articles__content">
                    <h4 class="articles__title"><?echo $arItem["NAME"]?></h4>
                    <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
                        <div class="articles__text"><?echo $arItem["PREVIEW_TEXT"];?></div>
                    <?endif;?>
                </div>
            </a>
        </div>
    <?endforeach;?>
</div>