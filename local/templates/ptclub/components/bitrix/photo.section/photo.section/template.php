<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="gallery">
    <?foreach($arResult["ROWS"] as $arItems):?>
        <?foreach($arItems as $arItem):?>
            <?if(is_array($arItem)):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));
                ?>
                <a href="<?=$arItem["PICTURE"]["SRC"]?>" class="gallery__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <img src="<?=$arItem["PICTURE"]["SRC"]?>" alt="<?=$arItem["PICTURE"]["ALT"]?>" class="gallery__img">
                <i class="gallery__icon ficon ficon_eye"></i>
            <?endif;?>
        <?endforeach;?>
    <?endforeach;?>
    </a>
</div>
