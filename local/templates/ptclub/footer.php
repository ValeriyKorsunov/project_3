<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>



        <?if($isIndexPage):?>
            </div>

            <div class="links">
                <div class="container">
                    <div class="links__list row">
                        <div class="col-lg-4">
                            <?$APPLICATION->IncludeComponent(
                                "dv:date.current",
                                "",
                                Array(
                                    "TEMPLATE_FOR_BUTTON" => "Узнать больше",
                                    "TEMPLATE_FOR_IMG" => "/local/templates/ptclub/temp/links/1.jpg",
                                    "TEMPLATE_FOR_TITLE" => "Занятия",
                                    "TEMPLATE_FOR_link" => "/"
                                )
                            );?>
                        </div>
                        <div class="col-lg-4">
                            <?$APPLICATION->IncludeComponent(
                                "dv:date.current",
                                "",
                                Array(
                                    "TEMPLATE_FOR_BUTTON" => "Узнать больше",
                                    "TEMPLATE_FOR_IMG" => "/local/templates/ptclub/temp/links/2.jpg",
                                    "TEMPLATE_FOR_TITLE" => "Корты",
                                    "TEMPLATE_FOR_link" => "/"
                                )
                            );?>
                        </div>
                        <div class="col-lg-4">
                            <?$APPLICATION->IncludeComponent(
                                "dv:date.current",
                                "",
                                Array(
                                    "TEMPLATE_FOR_BUTTON" => "Узнать больше",
                                    "TEMPLATE_FOR_IMG" => "/local/templates/ptclub/temp/links/3.jpg",
                                    "TEMPLATE_FOR_TITLE" => "Трененры",
                                    "TEMPLATE_FOR_link" => "/"
                                )
                            );?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="promo">
                <div class="promo__img-block" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/temp/promo/1.jpg);"></div>
                <div class="promo__container container">
                    <h1 class="promo__title">Pro Club</h1>
                    <h2 class="promo__subtitle">Лучший теннисный клуб Зеленограда</h2>
                    <div class="promo__text">Pro Club - профессиональный теннисный клуб, который представляет широкий спектр спортивных и оздоровительных услуг. Наши специалисты, обладающие высокой квалификацией и большим опытом, помогут Вам добиться значительных результатов как в профессиональном, так и любительском теннисе, а кроме того, быть всегда в отличной форме и прекрасном расположении духа. Мы любим теннис и хотим поделиться этим с Вами!</div>
                    <div class="promo__action">
                        <a href="" class="button button_primary button_width_300">Записаться</a>
                    </div>
                </div><!-- /.container -->
            </div>

            <div class="achievements">
                <div class="container">
                    <h2 class="achievements__title">Ваши достижения – наша гордость!</h2>
                    <div class="achievements__list row">
                        <div class="achievements__col col-lg-4">
                            <div class="achievements__item">
                                <i class="achievements__item-icon ficon ficon_person"></i>
                                <?$APPLICATION->IncludeComponent(
                                    "dv:your_progress",
                                    "",
                                    Array(
                                        "TEMPLATE_FOR_TEXT" => "научились играть в&nbsp;теннис в 2015 году в нашей академии",
                                        "TEMPLATE_FOR_TITLE" => "250 человек"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="achievements__col col-lg-4">
                            <div class="achievements__item">
                                <i class="achievements__item-icon ficon ficon_prize"></i>
                                <?$APPLICATION->IncludeComponent(
                                    "dv:your_progress",
                                    "",
                                    Array(
                                        "TEMPLATE_FOR_TEXT" => "выиграли наши ученики в 2015 году",
                                        "TEMPLATE_FOR_TITLE" => "36 турниров"
                                    )
                                );?>
                            </div>
                        </div>
                        <div class="achievements__col col-lg-4">
                            <div class="achievements__item">
                                <i class="achievements__item-icon ficon ficon_tennis"></i>
                                <?$APPLICATION->IncludeComponent(
                                    "dv:your_progress",
                                    "",
                                    Array(
                                        "TEMPLATE_FOR_TEXT" => "сбора было проведено в 2015 году, 2 из них совместно c игроками TOP 100 ATP и WTA",
                                        "TEMPLATE_FOR_TITLE" => "2 тренировочных"
                                    )
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?$APPLICATION->IncludeComponent(
                "bitrix:photo.section",
                "photo.section",
                Array(
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COMPONENT_TEMPLATE" => ".default",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "FIELD_CODE" => array(0=>"",1=>"",),
                    "FILTER_NAME" => "arrFilter",
                    "IBLOCK_ID" => "2",
                    "IBLOCK_TYPE" => "img_footer",
                    "LINE_ELEMENT_COUNT" => "5",
                    "MESSAGE_404" => "",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Фотографии",
                    "PAGE_ELEMENT_COUNT" => "10",
                    "PROPERTY_CODE" => array(0=>"",1=>"",),
                    "SECTION_CODE" => "",
                    "SECTION_ID" => $_REQUEST["SECTION_ID"],
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array(0=>"",1=>"",),
                    "SET_LAST_MODIFIED" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N"
                )
            );?>
        <?else:?>
            </div><!-- /.container -->

        <?endif;?>
        </main><!-- /.content -->
    </div>
    
    <footer class="footer">
        <div class="container">
            <div class="footer__top">
                <div class="row row-lg-center">
                    <div class="col-lg-4">
                        <a href="#js-modal-form" class="button button_primary js-modal-toggle">Хочу играть в теннис</a>
                    </div><!-- /.col -->
                    <div class="col-lg-3">
                        <? $APPLICATION->IncludeFile( SITE_DIR.'/include/address_footer.php',
                            array(),
                            array(MODE => "html")); ?>
                    </div><!-- /.col -->
                    <div class="col-lg-2">
                        <a href="tel:<?=file_get_contents('include/telephone_header.php');?>">
                            <? $APPLICATION->IncludeFile(SITE_DIR."/include/telephone_header.php",
                                array(),
                                array(MODE => "text")); ?>
                        </a>
                        <a href="mailto:<?=file_get_contents('include/mail_header.php');?>">
                            <? $APPLICATION->IncludeFile(SITE_DIR."/include/mail_header.php",
                                array(),
                                array(MODE => "text")); ?>
                        </a>
                    </div><!-- /.col -->
                    <div class="col-lg-3 text-right">
                        <div class="footer__social social">
                            <a href="" class="social__item ficon ficon_fb" title="Facebook"></a>
                            <a href="" class="social__item ficon ficon_vk" title="Вконтакте"></a>
                            <a href="" class="social__item ficon ficon_ok" title="Одноклассники"></a>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
            <div class="footer__bottom">
                <div class="row row-lg-center">
                    <div class="col-lg-6">
                        <? $APPLICATION->IncludeFile(SITE_DIR.'/include/footer__bottom.php',
                            array(),
                            array(MODE=>'html'));
                        ?>
                    </div><!-- /.col -->
                    <div class="col-lg-6 text-right">
                        <div class="footer__pixel">Создание сайта —<br>
                        компания <a href="" class="footer__link">«Пиксель Плюс»</a></div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div><!-- /.container -->
    </footer><!-- /.footer -->
    
    <!-- Modal form -->
    <form class="form modal mfp-hide zoom-anim-dialog" id="js-modal-form">
        <button title="Close (Esc)" type="button" class="modal__close mfp-close ficon ficon_close"></button>
    	<h2 class="modal__title">Теннис ждет Вас!</h2>
        <div class="modal__text">Начните играть уже сегодня. <br>Заполните форму и мы свяжемся с Вами в&nbsp;ближайшее время.</div>
        <div class="form__group form__group_required">
			<input id="name" type="text" placeholder="Ваше имя" required="" class="form__control" tabindex="10">
        </div>
        <div class="form__group form__group_required">
			<input id="tel" type="tel" placeholder="Телефон" required="" class="form__control" tabindex="20">
        </div>
        <div class="form__group form__group_submit">
			<button type="submit" class="button button_block button_primary" tabindex="30">Хочу играть в теннис</button>
        </div>
    </form>  
  </body>
</html>