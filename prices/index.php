<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>

        <h1>Цены</h1>
        <div class="box">
            <div class="price">
                <h2>Аренда корта (1 час)</h2>
                <table celpadding="0">
                    <tbody>
                    <tr>
                        <th rowspan="3">Будни</th>
                        <td>8:00 – 10:00</td>
                        <td>900 руб.</td>
                    </tr>
                    <tr>
                        <td>10:00 – 15:00</td>
                        <td>1 100 руб.</td>
                    </tr>
                    <tr>
                        <td>15:00 – 22:00</td>
                        <td>1 300 руб.</td>
                    </tr>
                    <tr>
                        <th rowspan="2">Выходные</th>
                        <td>9:00 – 17:00</td>
                        <td>1 300 руб.</td>
                    </tr>
                    <tr>
                        <td>17:00 – 22:00</td>
                        <td>1 100 руб.</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Услуги тренера</h2>
                <table celpadding="0">
                    <tbody>
                    <tr>
                        <td>Индивидуальное занятие (за 1 час)</td>
                        <td>1 200 руб.</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Групповые занятия</h2>
                <table celpadding="0">
                    <tbody>
                    <tr>
                        <td>8 занятий/мес.</td>
                        <td>5 900 руб.</td>
                    </tr>
                    <tr>
                        <td>12 занятий/мес.</td>
                        <td>7 100 руб.</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Аренда инвентаря</h2>
                <table celpadding="0">
                    <tbody>
                    <tr>
                        <td>Ракетка</td>
                        <td>100 руб.</td>
                    </tr>
                    <tr>
                        <td>Корзина мячей</td>
                        <td>150 руб.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>